<?php

/**
* yvTransliterate - Transliteration Plugin for Joomla! 1.5
* @version		$Id: yvtransliterate.php 16 2011-03-18 11:11:34Z yvolk $
* @package		yvTransliteratePlugin
* @copyright	2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class plgSystemyvTransliterate extends JPlugin {
	var $_initialized = false;
	var $_instanceId = 0;

	/**
	 * Constructor
	 */
	function plgSystemYvTransliterate(& $subject, $config) {
		parent :: __construct($subject, $config);
		$this->_instanceId = rand(1000, 9999);
		
		//$mainframe = JFactory::getApplication();
		//$message = 'yvTransliterate ' . $this->_instanceId . ' created.<br/>'; 
		//$mainframe->enqueueMessage($message, 'notice');
	}

  function onAfterInitialise()
  {
    if ($this->params->get('extend_table_content', true)) {
		$path = dirname(__FILE__).DS.'yvtransliterate'.DS.'table_content' ;
	    JTable::addIncludePath($path);
    }
  }

	function onAfterRoute() {
		$this->_initialize();

		if (class_exists('yvTransliterateHelper')) {
			$yvTransliterate = &yvTransliterateHelper::getInstance();
			if ($yvTransliterate->getDebug()) {
				$strFrom = $yvTransliterate->getConfigValue('debug_string', '');
				if (!empty($strFrom)) {
					$langTag = $yvTransliterate->getConfigValue('debug_lang_tag', '');
					$TransliterationMethodName = $yvTransliterate->getConfigValue('debug_transliteration_method', 'default');
					$strTo = $yvTransliterate->transliterate($strFrom, $langTag, 'en-GB', $TransliterationMethodName);
				}
			}
		}
		
	}

	// returns boolean Ok
	function _initialize() {
		$mainframe = JFactory::getApplication();
		if ($this->_initialized) {
			JError::raiseWarning( '1' , 'yvTransliterate instanceId=' . $this->_instanceId . ' was initialized already');
			return true;
		}
		//echo 'yvTransliterate ' . $this->_instanceId . ' initializes...<br/>';
		$this->_initialized = true;

		$yvTransliterate = null;
		
		//echo 'yvtransliterate Module constructor' . '<br/>';
		$Ok = false;

		if (!class_exists('yvTransliterateHelper')) {
			$path = JPATH_SITE . DS . 'plugins' . DS . 'system' . DS . 'yvtransliterate' . DS . 'yvtransliterate' . DS . 'helpers.php';
			if (file_exists($path)) {
				require_once ($path);
			}
		}
	
		if (class_exists('yvTransliterateHelper')) {
			$yvTransliterate = &yvTransliterateHelper::getInstance();
			$Ok = yvTransliterateHelper::Ok();
		}
		if (!$Ok) {
			// No yvTransliterate Component
		  $mainframe->enqueueMessage(
			  'yvTransliterate extension was not initialized',
		  	'error');
		}
		return ($Ok);
	}

	// Transliterate string
	function onTransliterate_transliterate(& $StringToTransliterate, $SourceLangTag, $DestinationLangTag = 'en-GB', $TransliterationMethodName = 'default') {
		if (class_exists('yvTransliterateHelper')) {
			$yvTransliterate = &yvTransliterateHelper::getInstance();
			$Ok = yvTransliterateHelper::Ok();
			if ($Ok) {
				$results = $yvTransliterate->transliterate($StringToTransliterate, $SourceLangTag, $DestinationLangTag, $TransliterationMethodName);
				$StringToTransliterate = $results;
				return true; 
			}
		}
	}
}
?>
