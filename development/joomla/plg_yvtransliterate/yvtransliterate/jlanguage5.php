<?php
/**
* @version		$Id: jlanguage5.php 17 2013-02-09 11:42:08Z yvolk $
* @package		yvTransliteratePlugin
* @copyright	2007-2008 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license GPL
*/

/*
 * Proxy class for JLanguage
 */
class yvJLanguage extends yvAutoForward 
{
	function transliterate($string)
	{
  	// Let's assume content language 
  	//   is the same as current User's language	
   	$langTag = $this->m_object->getTag(); 

		// Trigger onTransliterate_transliterate event 
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onTransliterate_transliterate', array(&$string, $langTag));

		// This line works (and calls m_object->transliterate) for PHP 5.2.6
		// but gives for PHP 5.2.4 'Fatal error: Call to undefined method yvAutoForward::transliterate()'
		// $string = parent::transliterate($string);
		
		// And this works for both tested versions of PHP 5.2.X
		$string = $this->m_object->transliterate($string);
		
		//$string .= '-yvJLanguage';
		return $string;
	}
}

/**
  * AutoForward baseclass for automatic forwarding of
  * method calls and member variables.
  * http://www.jansch.nl/tag/proxy/
  *
  * @author Peter C. Verhage
  * @author Martin Roest
  */
class yvAutoForward
{
  var $m_object;
 
  /**
   * Constructor.
   *
   * @param Object $object
   */
  function __construct(&$object)
  {
    $this->m_object = $object;
  }
 
  /**
   * Returns the forwarded object.
   */
  function &__getObject()
  {
    return $this->m_object;
  }
 
  /**
   * Forward method calls.
   *
   * @param String $method method name
   * @param Array $args method arguments
   * @return Unknown method return value
   */
  function __call($method, $args)
  {
   return call_user_func_array(array($this->m_object, $method), $args);
  }
 
  /**
   * Forward property set.
   *
   * @param String $name property name
   * @param Unknown $value property value
   */
  function __set($name, $value)
  {
    $this->m_object->$name = $value;
  }
 
  /**
   * Forward property get.
   *
   * @param String $name, property name
   * @return Unknown
   */
  function __get($name)
  {
    return $this->m_object->$name;
  }
}


?>
