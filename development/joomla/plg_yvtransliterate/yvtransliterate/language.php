<?php
/**
 * @version		$Id: language.php 17 2013-02-09 11:42:08Z yvolk $
 * @package		yvTransliteratePlugin
 * @copyright	2007-2008 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 */

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

require_once (JPATH_SITE.DS.'libraries'.DS.'joomla'.DS.'language'.DS.'language.php');

/**
 * Languages/translation handler class
 */
class yvLanguage extends JObject
{
	var $_debug = false;
	var $_debugTr = false;

	/**
	 * Array holding the language metadata
	 *
	 * @var array
	 * @access protected
	 */
	var $_metadata 	= null;
	var $_HasTransliteration = false;

	/**
	 * The language tag (Identifying string of the language)
	 *
	 * @var string
	 * @access protected
	 */
	var $_langTag = null;

	/**
	 * Constructor activating the default information of the language
	 *
	 * @access protected
	 */
	function __construct($langTag)
	{
		if (class_exists('yvTransliterateHelper')) {
			$yvTransliterate = &yvTransliterateHelper::getInstance();
			$this->_debug = $yvTransliterate->getDebug();
			//echo 'yvTransliterate, langTag=' . $langTag . '; debug=' . $this->_debug . '<br />';
		}
		$this->setLanguage($langTag);
	}

	/**
	 * Get for the language tag (as defined in RFC 3066)
	 *
	 * @access	public
	 * @return	string The language tag
	 * @since	1.5
	 */
	function getTag() {
		return $this->_langTag;
	}

	/**
	 * Set the language attributes to the given language
	 *
	 * Once called, the language still needs to be loaded using yvLanguage::load()
	 *
	 * @access	public
	 * @param	string	$langTag	Language tag
	 */
	function setLanguage($langTag)
	{
		$this->_langTag		= $langTag;
		$this->_metadata	= $this->getMetadata($this->_langTag);

	}

	function HasTransliteration() {
		return $this->_HasTransliteration;
	}

	/**
	 * Returns a associative array holding the metadata
	 *
	 * @access public
	 * @param string	The tag of the language
	 * @return mixed	If $langTag exists return key/value pair with the language metadata,
	 *  				otherwise return NULL
	 */

	function getMetadata($langTag)
	{
		$path = JLanguage::getLanguagePath(JPATH_ADMINISTRATOR, $langTag);
		$file = $langTag.'.yvtransliterate.xml';

		$result = null;
		$fullname = $path.DS.$file;
		$found = is_file($fullname);
		if ($this->getDebug()) {
			$message = get_class($this) . ': Language Tag="' . $langTag . '", File="' . $fullname . '" ' . ($found ? 'found' : 'not found') . '<br/>';
			if (!empty($message)) {
				$mainframe = JFactory::getApplication();
				$mainframe->enqueueMessage($message, ($found ? 'notice' : 'error'));
			}
		}
		if($found) {
			$result = yvLanguage::_parseXMLLanguageFile($fullname);
		}

		return $result;
	}

	/**
	 * Parse XML file for language information
	 *
	 * @access public
	 * @param string	$path	 Path to the xml files
	 * @return array	Array holding the found metadata as a key => value pair
	 */
	function _parseXMLLanguageFile($path)
	{
		$message = '';
		$Ok = true;
		jimport('joomla.factory');
		$xml = JFactory::getXMLParser('Simple');
		if (!$xml->loadFile($path)) {
			$message .= get_class($this) . ': File="' . $path . '" was not loaded<br/>';
			$Ok = false;
		}

		// Check that it's am metadata file
		if ($Ok && $xml->document->name() != 'metafile') {
			$message .= get_class($this) . ': File="' . $path . '" has now "metafile" element ???<br/>';
			$Ok = false;
		}

		$metadata = array ();

		if ($Ok) {
			//if ($xml->document->attributes('type') == 'language') {

			foreach ($xml->document->metadata[0]->children() as $child) {
				switch ($child->name()) {
					case 'transliterations' :
						$transliterations = array();
						foreach ($child->children() as $transliterationMetadata) {
							$transliteration = new yvTransliteration($transliterationMetadata);
							if ($this->getDebug()) {
								$message .= 'Transliteration method loaded: "' . $transliteration->getName() . '"';
							}
							$transliterations[$transliteration->getName()] = $transliteration;
							$this->_HasTransliteration = true;
						}
						$metadata[$child->name()] = $transliterations;
						break;
					default :
						$metadata[$child->name()] = $child->data();
				}
			}
			//}
		}
		if (!empty($message) || $this->getDebug()) {
			$message = 'Parsing XMLLanguageFile "' . $path . '"<br />' . $message;
		}
		if (!empty($message)) {
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		}
		if (!$Ok) { $metadata = null; }
		return $metadata;
	}

	/**
	 * Transliterate string
	 *
	 * @access public
	 * @param string	$StringToTransliterate
	 * @param string  $DestinationLangTag
	 * @param string  $TransliterationMethodName
	 * @return string	Transliterated string
	 */
	function transliterate( $StringToTransliterate, $DestinationLangTag = 'en-GB', $TransliterationMethodName = 'default')
	{
		$message = '';
		$strOut = $StringToTransliterate;
		$done = false;

		if (!$this->_HasTransliteration) {
			if ($this->getDebug()) {
				$message .= 'No transliterations - nothing to do<br />';
			}
			// return input string without changes
			$done = true;
		}
		if (!$done && $this->_langTag == $DestinationLangTag) {
			if ($this->getDebug()) {
				$message .= 'Same language - nothing to do<br />';
			}
			// return input string without changes
			$done = true;
		}

		$transliteration = null;
		if (!$done) {
			$sameLanguage = null; //
			// find yvTransliteration object
			foreach ($this->_metadata['transliterations'] as $trans) {
				if ($this->getDebug()) {
					$message .= 'Found destination language: ' . $trans->getDestinationLangTag() . '; needed: ' . $DestinationLangTag . '<br />';
				}
				if ($trans->getDestinationLangTag() == $DestinationLangTag) {
					if (!$sameLanguage) {
						$sameLanguage = $trans;
					}
					if ($TransliterationMethodName == 'default' ) {
						if ($this->getDebug()) {
							$message .= 'First transliteration is default<br />';
						}
						$transliteration = $trans;
						break;
					}
					if ($trans->getName() == $TransliterationMethodName) {
						if ($this->getDebug()) {
							$message .= 'Exact match found for Transliteration name="' . $TransliterationMethodName . '"<br />';
						}
						$transliteration = $trans;
						break;
					}
				}
			}
			if (!$transliteration) {
				if ($this->getDebug()) {
					$message .= 'Transliteration name "' . $TransliterationMethodName . '" was not found<br />';
				}
			}
		}

		if ($transliteration) {
			$strOut = $transliteration->transliterate($StringToTransliterate);
		}

		if (!empty($message)) {
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage($message, 'notice');
		}
		return $strOut;
	}

	/**
	 * Set the Debug property
	 *
	 * @access public
	 */
	function setDebug($debug) {
		if ($debug) {
			$this->_debug = $debug;
		}
	}

	/**
	 * Get the Debug property
	 *
	 * @access public
	 * @return boolean True is in debug mode
	 */
	function getDebug() {
		return $this->_debug;
	}

}  // yvLanguage Class

/**
 * Transliteration class
 * Private class for yvLanguage class,
 *   yvLanguage class holds information about Source Language of The transliteration.
 *
 * @author		Yuri Volkov <yvolk@yurivolkov.com>
 */
Class yvTransliteration extends JObject
{
	var $_debug = false;

	// name of the Transliteration
	var $_name = null;

	var $_description	= null;

	/**
	 * The language tag (Identifying string of the language)
	 *
	 * @var string
	 * @access private
	 */
	var $_destinationLangTag = null;

	/**
	 * Transliteration table.
	 * Two arrays of strings. Both arrays are of the same size.
	 *
	 * @var array()
	 * @access private
	 */
	var $_tableFrom = array();
	var $_tableTo = array();

	/**
	 * Constructor loading transliteration metadata
	 * is supposed to be called from yvLanguage Class
	 * @access protected
	 */
	function __construct($metadata)
	{

		if ($this->_debug) {
			echo 'yvTransliteration::__construct, metadata:<br/>';
			foreach ($metadata->children() as $row) {
				echo "&nbsp;&nbsp;Name: " . $row->name() . "; Data:'" . $row->data() . "'<br />";
			}
			//echo '<hr/><pre>' . print_r($metadata, true) . '</pre><hr/>';
		}

		$this->_destinationLangTag = $metadata->destinationLangTag[0]->data();
		// echo $this->_destinationLangTag;
		// echo " point 2 <br />";

		$this->_name = $metadata->name[0]->data();
		$this->_description = $metadata->description[0]->data();

		$table = array();
		foreach ($metadata->table[0]->children() as $row) {
			$from = $row->from[0]->data();
			if (strlen($from) > 0) {
				$this->_tableFrom[] = $from;
				$this->_tableTo[] = $row->to[0]->data();
			}
		}
	}

	function getDestinationLangTag() {
		return $this->_destinationLangTag;
	}

	function getName() {
		return $this->_name;
	}

	function getDescription() {
		return $this->_description;
	}

	/**
	 * Transliterate string
	 *
	 * @access protected (from yvLanguage class only)
	 * @param string	$StringToTransliterate
	 * @return string	Transliterated string
	 */
	function transliterate( $StringToTransliterate)
	{
		$pos = 0;
		$pos_end = strlen($StringToTransliterate); // ??maybe JString::strlen...
		$strOut = "";

		while ($pos < $pos_end) {
			// find substitution for the substring, starting with $pos position
			$found = false;
			for ($ind = 0; $ind < count($this->_tableFrom); $ind++) {
				$str1 = $this->_tableFrom[$ind];
				$len1 = strlen($str1);
				if (substr($StringToTransliterate, $pos, $len1) == $str1) {
					// case sensitive match found
					$found = true;
					$strOut .= $this->_tableTo[$ind];
					$pos += $len1;
					break;
				}
			}
			if (!$found) {
				// copy one char from input to output and move one char forward
				$strOut .= substr($StringToTransliterate, $pos, 1);
				$pos += 1;
			}
		}  // while

		return $strOut;
	}

} // Class yvTransliteration
?>
