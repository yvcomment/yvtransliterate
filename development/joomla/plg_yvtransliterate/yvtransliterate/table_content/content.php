<?php
/**
* This is extended class for Joomla! 'JTableContent' 
* @version		$Id: content.php 17 2013-02-09 11:42:08Z yvolk $
*/

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

// Read file with original JTableContent class
// and rename that class to: JTableContent_original
$path = JPATH_SITE .DS. 'libraries' .DS. 'joomla' .DS. 'database' .DS. 'table' .DS. 'content.php';
$str1 = file_get_contents($path);
$str1 = str_replace('JTableContent', 'JTableContent_original', $str1);
$str1 = str_replace('<?php', '', $str1);
$str1 = str_replace('?>', '', $str1);
eval($str1);

/**
 * Content table
 */
class JTableContent extends JTableContent_original
{
	var $_yvdebug = false;	


	function check()
	{
		$Ok = true;
		$return = true;
		$message = '';
		if ($this->_yvdebug) {
		  $message .= 'JTableContent_2::check() started <br/>';
		}
		
		if(empty($this->alias)) {
			$this->alias = $this->title;
			// 2008-07-17 Added by yvolk ---------------------
			// As first solution for transliteration
			// 1. Find Content Language 
			$form = new JRegistry('');
			$form->loadINI($this->attribs);
			// The langTag may be empty string, see 
			$langTag = $form->get('language');
			// 2. Trigger onTransliterate_transliterate event 
			$dispatcher = JDispatcher::getInstance();
			$dispatcher->trigger('onTransliterate_transliterate', array(&$this->alias, $langTag));
			//------------------------------------------------
		}
		$return = parent::check();

		if (!empty($message)) {
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		}
		return $return;
	}
}
