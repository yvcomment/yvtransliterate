<?php
/*
 * yvTransliterate - Transliteration Plugin for Joomla! 1.5
 * @version		$Id: helpers.php 17 2013-02-09 11:42:08Z yvolk $
 * @package		yvTransliteratePlugin
 * @copyright	2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 */
define('yvTransliterateVersion', '2.0.0');
define('JPATH_SITE_YVTRANSLITERATE', dirname(__FILE__) );

// Contains global yvTransliterate parameters etc.
class  yvTransliterateHelper extends JObject {
	//------------------------------------------------------------
	// Common attributes - the same for all instances of yvTransliterate

	private static $_Ok = true; // if false - error state: try to be quiet

	var $_debug = false;
	var $_pluginParams = null;

	var $_extendTableContent = false;
	var $_extendJLanguage = false;

	var $_nLanguages = 0;
	var $_aLanguages = array();
	var $_default_content_lang_tag = '';

	/*
	 * Initialize common attributes - the same for all instances of yvTransliterate
	 */
	function __construct() {
		$mainframe = JFactory::getApplication();
		$Ok = true;
		$message = '';

		// Get Plugin info
		$pluginData = JPluginHelper::getPlugin('system', 'yvtransliterate');
		//echo $paramName . '; ' . ($this->_pluginData ? 'plugin loaded' : 'plugin NOT loaded') . '<br/>';
		if ($pluginData) {
			$this->_pluginParams = new JRegistry($pluginData->params );
		}
		$this->_debug = $this->getConfigValue('debug', '1');
		$this->_default_content_lang_tag = $this->getConfigValue('default_content_lang_tag', '');
		
		$lang = JFactory :: getLanguage();
		$lang->load('plg_system_yvtransliterate', JPATH_ADMINISTRATOR);

		if ($Ok) {
			require_once (JPATH_SITE_YVTRANSLITERATE.DS.'language.php');
			$this->_extendTableContent = (bool) $this->getConfigValue('extend_table_content', true);
			$this->_extendJLanguage = (bool) $this->getConfigValue('extend_jlanguage', true);
		}

		if ($Ok && $this->_extendTableContent) {
			// It is too late to do this here...
			// JTable::addIncludePath(JPATH_SITE_YVTRANSLITERATE.DS.'table_content');
		}

		if ($Ok && $this->_extendJLanguage) {
			require_once (JPATH_SITE_YVTRANSLITERATE.DS.'jlanguage5.php');
			// Create proxy to existing JLanguage object
			$lang_new = new yvJLanguage($lang);
			// Replace old object with proxy
			// In Joomla! 1.6 this is public static variable
			JFactory::$language = $lang_new;
		}
			
		if (!$Ok) {
			$this->_Ok = false;
		}
		if (!empty($message)) {
			$message .= $this->_textSignature();
			$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		}
	}

	/**
	 * Transliterate string
	 *
	 * @access public
	 * @param string	$StringToTransliterate
	 * @param string  $SourceLangTag
	 * @param string  $DestinationLangTag
	 * @param string  $TransliterationMethodName
	 * @return string	Transliterated string
	 */
	function transliterate( $StringToTransliterate, $SourceLangTag = '', $DestinationLangTag = 'en-GB', $TransliterationMethodName = 'default')
	{
		$mainframe = JFactory::getApplication();

		$message = '';
		$result = '';
		$done = false;
		if ($this->getDebug()) {
			$message .= 'Transliterate:<br/>';
		}
		$SourceLangTag = trim($SourceLangTag);
		if ($SourceLangTag == '') {
			if ($this->getDebug()) {
				$message .= 'Content Language is not defined. ';
			}
			switch ($this->_default_content_lang_tag) {
			case '':
				// Let's assume content language
				//   is the same as current User's language
				$Lang1 = &JFactory::getLanguage();
				$SourceLangTag = $Lang1->getTag();
				if ($this->getDebug()) {
					$message .= 'Defaults to current language: ';
				}
				break;
			default:
				$SourceLangTag = $this->_default_content_lang_tag;
				if ($this->getDebug()) {
					$message .= 'Defaults to user specified language: ';
				}
			}
			if ($this->getDebug()) {
				$message .= '"' . $SourceLangTag . '"<br/>';
			}
		}
		
		if (empty($StringToTransliterate)) {
			$done = true;
			if ($this->getDebug()) {
				$message .= '- Nothing to do (empty string)<br/>';
			}
		}
		if (!$done) {
			if ($SourceLangTag == $DestinationLangTag) {
				// echo '(nothing to do)<br />';
				// return input string without changes
				$result = $StringToTransliterate;
				$done = true;
				if ($this->getDebug()) {
					$message .= '- Nothing to do (same languages)<br/>';
				}
			}
		}
		if (!$done) {
			$yvlanguage1 = null;

			// find yvLanguage object
			foreach ($this->_aLanguages as $yvlanguage2) {
				if ($yvlanguage2->getTag() == $SourceLangTag) {
					$yvlanguage1 = $yvlanguage2;
					break;
				}
			}
			if (!$yvlanguage1) {
				if ($this->getDebug()) {
					$message .= 'Loading new language="' . $SourceLangTag . '"<br />';
				}
				$yvlanguage1 = New yvLanguage($SourceLangTag);
				$this->_aLanguages[$SourceLangTag] = $yvlanguage1;
				$this->_nLanguages += 1;
			}

			if ($yvlanguage1) {
				$result =  $yvlanguage1->transliterate($StringToTransliterate, $DestinationLangTag, $TransliterationMethodName);
			} else {
				// echo '(return input string without changes)<br />';
				$result = $StringToTransliterate;
			}
			$done = true;
		}

		if ($this->getDebug()) {
			$message .= 'From lang="' . $SourceLangTag
			. '", To lang="' . $DestinationLangTag
			. '", method="' . $TransliterationMethodName
			. '", source string: <br/>"' . $StringToTransliterate . '"';
			$message .= '<br/>transliterated string: <br/>"' . $result . '"';
		}
			
		if (strlen($message) > 0) {
			$message .= $this->_textSignature();
			$mainframe->enqueueMessage($message, ('notice'));
		}
		return $result;
	}

	public static function Ok() {
		return self::$_Ok;
	}

	/*
	 * Returns value of Component/Plugin parameter
	 * from Common attributes
	 */
	function getConfigValue($paramName = '', $default = '') {
		$value = $default;

		switch ($paramName) {
			case 'access':
				if ($this->_pluginData) {
					// '1' means some lowest level...
					$value = 1;
				} else {
					// if Plugin is not loaded, then Access is denied
					$value = 0; //999;
				}
				break;
			default:
				if ($this->_pluginParams) {
					$value = $this->_pluginParams->get($paramName, $default);
				}
		}
		//echo 'getConfigValue param="' . $paramName . '", value="' . $value . '"<br/>';

		return $value;
	}

	function getSiteURL() {
		$mainframe = JFactory::getApplication();
		$uri = JFactory::getURI();
		return ($mainframe->isAdmin() ? $uri->root() : JURI :: base());
	}

	// Credits for install/uninstall
	function Credits() {
		?>
<fieldset class="adminform"><legend><?php echo JText::_( 'CREDITS'); ?></legend>
<table class="admintable" width="100%">
	<tr>
		<th width="40%"><?php echo JText::_( 'CREDITS_NAMES'); ?></th>
		<th width="60%"><?php echo JText::_( 'CONTRIBUTION'); ?></th>
	</tr>
	<tr>
		<td class="name"><a href="http://yurivolkov.com/index_en.html"
			target="_blank">Yuri Volkov</a></td>
		<td><?php echo JText::_( 'FOUNDER'); ?>, <?php echo JText::_( 'PROJECT_LEADER' ); ?>
		&amp; <?php echo JText::_( 'DEVELOPER'); ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.ecsoftware.cz" target="_blank">Pavol
		Goga, ecSoftware</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Slovak)'; ?></td>
	</tr>

	<!-- Before 2008-12-12 -->

	<!--
				<tr>
					<td class="name">
					<a href="http://www." target="_blank">Some man</a>
					</td>
					<td>
					  <?php echo JText::_( 'TRANSLATOR') . ' (...)'; ?>
					</td>
				</tr>
				-->
</table>
</fieldset>
					  <?php
	}

	// Signature of this Extension
	function _textSignature() {
		$message = '<br/>-- <br/>' .
		'<a href="http://yurivolkov.com/Joomla/yvTransliterate/index_en.html" target="_blank">' .
		'yvTransliterate extension</a>, version="' .
		yvTransliterateVersion . '"';
		return $message;
	}

	function getDebug() {
		return $this->_debug;
	}


	/**
	 * Returns a reference to the global object of this class, only creating it
	 * if it doesn't already exist.
	 */
	static function &getInstance()
	{
		static $instance;

		if (!$instance)
		{
			$instance = new yvTransliterateHelper();
		}
		return $instance;
	}

	/**
	 * Get the value from the comment metadata, which is an INI formatted string.
	 * Empty input will return null.
	 */
	function getValueFromIni($metadata_str, $key = '')
	{
		if(empty($metadata_str)) {
			return null;
		}

		$metadata_registry = new JRegistry();
		$metadata_registry->loadINI($metadata_str);
		$value = $metadata_registry->getValue($key);
		switch ($key) {
			case 'created_by_link':
			case 'created_by_email':
				$value = urldecode(html_entity_decode($value));
				break;

		}
		//echo '"' .$key . '"-"' . $value . '"<br />';
		return $value;
	}

	/**
	 *  Database helper functions
	 *  I didn't put them inside yvTransliterateHelper to reduce calling code
	 */
	function DLookup($Expression, $Domain, $Criteria) {
		$db =& JFactory::getDBO();
		return $this->DLookup_db($db, $Expression, $Domain, $Criteria);
	}

	function DLookup_db(&$db, $Expression, $Domain, $Criteria = '') {
		$debug = false;
		$sOut = null;
		$query = 'SELECT';
		$query .=  ' (' . $Expression . ') As Expr1';
		$query .= ' FROM ' . $Domain;
		if ($Criteria) {
			$query .= ' WHERE (' . $Criteria . ')';
		}
		$row = null;
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($db->getErrorNum() > 0) {
			echo $this->printDbErrors($db);
		}
			
		if (is_object($row))	{
			if ($debug) {
				echo get_class($row). '<br />';
				echo 'object_vars=' . '<br />';
				foreach(get_object_vars($row) as $key=>$val) {
					echo $key . '="' . $val . '"<br />';
				}
				echo 'query="' . $query . '"<br />';
			}
			$sOut = $row->Expr1;
		}
		return $sOut;
	}

	function TableExists($pattern, $prefix='#__') {
		$db =& JFactory::getDBO();
		return $this->TableExists_db($db, $pattern, $prefix);
	}

	function TableExists_db(&$db, $pattern, $prefix='#__') {
		$Exists = false;
		$debug = false;
		// JDatabase class doesn't substitute prefix if table name is in single (or double) quotes
		$pattern2 = str_replace( $prefix, $db->getPrefix(), $pattern);
		// '_' is a literal and not part of the pattern
		$pattern2 = str_replace( '_', '\_', $pattern2);
		$query = 'SHOW TABLES LIKE \'' . $pattern2 . '\'';
		if ($debug) echo 'query="' . $query . '"<br />';;
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($db->getErrorNum() == 0) {
			if (is_object($row))	{
				if ($debug) {
					$ind1=1;
					foreach(get_object_vars($row) as $key=>$val) {
						echo $ind1 . '. "'. $key . '"="' . $val . '"<br />';
						$ind1 +=1;
					}
				}
				$Exists = true;
			}
		} elseif ($debug) echo $this->printDbErrors($db);
		if ($debug) echo ($Exists? 'true' : 'false') . '<br/>';
		return $Exists;
	}

	function printDbErrors($db) {
		$out = null;
		if ($db->getErrorNum() > 0)	{
			$out = '<p>Database errors:' . '<br />';
			$errors[] = array('msg' => $db->getErrorMsg(), 'sql' => $db->_sql);
			foreach( $errors as $error) {
				$out .= 'Error message: "' . $error['msg'] . '"<br />';
				$out .= '  SQL="' . $error['sql'] . '"<br /><hr />';
			}
			$out .= '</p>';
		}
		return $out;
	}

}
// End of yvTransliterateHelper class
?>
