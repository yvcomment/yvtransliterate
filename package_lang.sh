#!/bin/bash
PROJECTNAME="yvTransliterate"
# ----------------------------------------------------------------------------
# $Id: package_lang.sh 6 2009-08-30 16:23:21Z yvolk $
# Package $PROJECTNAME using "phing",
# see http://phing.info/
# ---------------------------------------------------------------------------- 
function pause {
	echo ""
	read -p "Please press Enter to continue..." nothing
}

echo "-------------------------"
echo "Packaging $PROJECTNAME Language Pack"

MYROOT=$(dirname $0)
echo "Root Dir: $MYROOT"
cd $MYROOT
echo -n "Current dir: "
pwd

read -p "Language tag (e.g. de-DE es-ES ru-RU): " LANGUAGE
read -p "Version number (e.g. 1.24.000): " VERSION

if [ "$VERSION" = "" ] || [ "$LANGUAGE" = "" ]; then
	echo "Nothing to do"
else
	phing package_lang -Dver=$VERSION -Dlang=$LANGUAGE
fi

echo -n "-------------------------"
pause
exit 0
 