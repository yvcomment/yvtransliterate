#!/bin/bash
PROJECTNAME="yvTransliterate"
# ----------------------------------------------------------------------------
# $Id: package.sh 6 2009-08-30 16:23:21Z yvolk $
# Package $PROJECTNAME using "phing",
# see http://phing.info/
# ---------------------------------------------------------------------------- 
function pause {
	echo ""
	read -p "Please press Enter to continue..." nothing
}

echo "-------------------------"
echo "Packaging $PROJECTNAME extension"

MYROOT=$(dirname $0)
echo "Root Dir: $MYROOT"
cd $MYROOT
echo -n "Current dir: "
pwd

read -p "Version number (e.g. 1.01.002): " VERSION

if [ "$VERSION" = "" ]; then
	echo "Nothing to do"
else
	phing package -Dver=$VERSION
fi

echo -n "-------------------------"
pause
exit 0
 
